package Tampilan_GUI_UKM;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import Tampilan_GUI_UKM.Mahasiswa;
import Tampilan_GUI_UKM.Masyarakat;
import Tampilan_GUI_UKM.Penduduk;
import Tampilan_GUI_UKM.UKM;
public class mainFrame extends JFrame implements ActionListener{

    private JMenuBar menuBar;

    private JMenuItem menuItemEdit_tambahMhs;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM;

    private JMenuItem menuItemFile_lihatData;
    private JMenuItem menuItemFile_exit;

    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;

    public static UKM ukm = new UKM();
    public static Mahasiswa mhs = new Mahasiswa();
    public static Masyarakat masya = new Masyarakat();
    public static Penduduk[] anggota = new Penduduk[5];
    public static int jumlah = 0;  
    public mainFrame() {
        initComponents();
    }
    private void initComponents() {
        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMhs = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItemEdit_tambahUKM = new JMenuItem("Tambah UKM");
        menuItemFile_lihatData = new JMenuItem("Lihat Data");
        menuItemFile_exit= new JMenuItem("Exit");

        menu_Edit.add(menuItemEdit_tambahMhs);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);
        
        
        menu_File.add(menuItemFile_lihatData);
        menu_File.add(menuItemFile_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        menuItemFile_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menuItemEdit_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMahasiswa_dialog addMhs = new tambahMahasiswa_dialog();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });
        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMasyarakat_dialog addMasyarakat = new tambahMasyarakat_dialog();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });
        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahUKM_dialog addUKM = new tambahUKM_dialog();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });       
        menuItemFile_lihatData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mahasiswaTable_dialog mhsTable = new mahasiswaTable_dialog();
                mhsTable.setSize(300, 400);
                mhsTable.setVisible(true);
            }
        });
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame main = new mainFrame();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}
